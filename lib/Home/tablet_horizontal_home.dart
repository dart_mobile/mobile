import 'package:flutter/material.dart';
import '../Game.dart';

class TabletHomeHorizontal extends StatefulWidget {
  const TabletHomeHorizontal({Key? key}) : super(key: key);

  @override
  State<TabletHomeHorizontal> createState() => _TabletHomeHorizontalState();
}

class _TabletHomeHorizontalState extends State<TabletHomeHorizontal> {
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Container(
      color: Colors.lightBlue.shade900,
      child: Column(
        children: [
          Expanded(
            flex: 2,
            child: Center(
              child: Text(""),
            ),
          ),
          Expanded(
            flex: 2,
            child: Center(
              child: Text("MATH SPEED",
                  style: TextStyle(
                      fontSize: height * 0.12,
                      fontWeight: FontWeight.bold,
                      color: Colors.white)),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: height * 0.015),
            child: Image(
              height: height * 0.5,
              image: NetworkImage(
                  'https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.15752-9/333981080_905446673908533_3188371953273655919_n.png?_nc_cat=107&ccb=1-7&_nc_sid=ae9488&_nc_ohc=S_qD87kQEy0AX9XZH1m&_nc_ht=scontent.fbkk5-7.fna&oh=03_AdSeIfD6PFHadKPD1SsAhKi7LNRfJEM7TkTK-edjcsSd9Q&oe=6426C871'),
            ),
          ),
          Expanded(
            flex: 2,
            child: Center(
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.lightBlue.shade700,
                    fixedSize: Size(height * 0.35, height * 0.1),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10))),
                child: Text(
                  'Start',
                  style: TextStyle(fontSize: height * 0.05),
                ),
                onPressed: () {
                  _navigateToNextScreen(context);
                },
              ),
            ),
          ),
          const Expanded(
            flex: 2,
            child: Center(
              child: Text(""),
            ),
          ),
        ],
      ),
    );
  }

  void _navigateToNextScreen(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => MainPage(),
      ),
    );
  }
}
