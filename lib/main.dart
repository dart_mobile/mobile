import 'package:flutter/material.dart';
import 'HomPage.dart';
import 'package:device_preview/device_preview.dart';

void main() {

  runApp(
    DevicePreview(
      enabled: true,
      builder: (context) => const MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        home: HomePage(),
      ),
    ),
  );
}
