import 'package:flutter/material.dart';

class buildAns extends StatelessWidget {
  final textFillWidth;
  final textFillHeight;
  final fontSize;

  buildAns(
      {required this.textFillWidth,
      required this.textFillHeight,
      this.fontSize});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.only(left: 30, right: 30),
        child: Container(
          width: textFillWidth,
          height: textFillHeight,
          decoration: BoxDecoration(
            color: Colors.lightBlue.shade700,
            borderRadius: BorderRadius.circular(15),
          ),
          child: TextField(
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.all(12.5),
              alignLabelWithHint: true,
            ),
            style: TextStyle(fontSize: fontSize),
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}
