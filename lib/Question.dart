import 'package:flutter/material.dart';

class buildQuestion extends StatefulWidget {
  final fontSize;
  final int firstNumber;
  final int secondNumber;

  buildQuestion(
      {Key? key,
      required this.fontSize,
      required this.firstNumber,
      required this.secondNumber})
      : super(key: key);

  @override
  State<buildQuestion> createState() => _buildQuestionState();
}

class _buildQuestionState extends State<buildQuestion> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        "${widget.firstNumber} + ${widget.secondNumber} = ?",
        style: TextStyle(color: Colors.white, fontSize: widget.fontSize),
      ),
    );
  }
}
