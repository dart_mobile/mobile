import 'package:flutter/material.dart';

class Dialog_Horizontal extends StatelessWidget {
  final VoidCallback onTap;

  const Dialog_Horizontal({Key? key,required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    context=context;
    return AlertDialog(
      content: Container(
        height: height * 0.3,
        width: width * 0.4,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.lightBlue.shade700,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              'Incorrect!',
              style: TextStyle(fontSize: width * 0.03,color: Colors.white,fontWeight: FontWeight.bold),
            ),
            GestureDetector(
              onTap: onTap,
              child: Container(
                padding: EdgeInsets.all(width*0.005),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.grey.shade100,
                ),
                child: Icon(Icons.close,color: Colors.lightBlue.shade700),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

