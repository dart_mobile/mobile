import 'package:flutter/material.dart';
import 'package:high_speed_math/reponsive_layout.dart';
import 'Home/desktop_home.dart';
import 'Home/mobile_horizontal_home.dart';
import 'Home/mobile_vertical_home.dart';
import 'Home/tablet_horizontal_home.dart';
import 'Home/tablet_vertical_home.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ResponsiveLayout(
        mobileBodyVertical: MobileHomeVertical(),
        mobileBodyHorizontal: MobileHomeHorizontal(),
        tabletBodyVertical: TabletHomeVertical(),
        tabletBodyHorizontal: TabletHomeHorizontal(),
        desktopBody: DesktopHome(),
      ),
    );
  }
}
