import 'package:flutter/material.dart';
import 'package:high_speed_math/GameUi_tablet_horizontal.dart';
import 'package:high_speed_math/MainGame/tablet_horizontal.dart';

import '../AppBarMath.dart';
import '../GameUi_tablet_vertical.dart';
import '../Question.dart';
import '../Text.dart';
import '../Numpad.dart';

class TabletBodyVertical extends StatelessWidget {
  final ValueChanged incrementCounter;
  final ValueChanged randomNumber;
  final int score;
  final int firstNumber;
  final int secondNumber;

  const TabletBodyVertical(
      {Key? key,
      required this.score,
      required this.incrementCounter,
      required this.firstNumber,
      required this.secondNumber,
      required this.randomNumber})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    final fontSize = screenHeight * 0.035;
    final aspectRatio = (screenHeight / screenWidth) * 2;
    final paddingSize = (screenHeight / screenWidth) * 8;
    final textFillWidth = screenWidth * 0.85;
    final textFillHeight = screenHeight * 0.08;
    final appbarHeight = screenHeight * 0.05;
    final appbarWidth = screenHeight * 0.05;
    final textInput = screenHeight * 0.04;

    if (screenWidth > screenHeight) {
      return BuildGameTabletHorizontal(
        fontSize: fontSize,
        firstNumber: firstNumber,
        secondNumber: secondNumber,
        randomNumber: randomNumber,
        appbarWidth: appbarWidth,
        score: score,
        textFillHeight: textFillHeight,
        textFillWidth: textFillWidth,
        paddingSize: paddingSize,
        aspectRatio: aspectRatio,
        appbarHeight: appbarHeight,
        incrementCounter: incrementCounter,
      );
    } else {
      return BuildGameTabletVertical(
        fontSize: fontSize,
        firstNumber: firstNumber,
        secondNumber: secondNumber,
        randomNumber: randomNumber,
        appbarWidth: appbarWidth,
        score: score,
        textFillHeight: textFillHeight,
        textFillWidth: textFillWidth,
        paddingSize: paddingSize,
        aspectRatio: aspectRatio,
        appbarHeight: appbarHeight,
        incrementCounter: incrementCounter,
      );
      //   Scaffold(
      //   appBar: PreferredSize(
      //     preferredSize: Size.fromHeight(appbarHeight),
      //     child: buildAppBar(
      //       appbarWidth: appbarWidth,
      //       appbarHeight: appbarHeight,
      //       score: score,
      //     ),
      //   ),
      //   body: Container(
      //     color: Colors.lightBlue.shade900,
      //     child: Column(
      //       children: <Widget>[
      //         // Expanded(
      //         //   flex: 3,
      //         //   child: buildQuestion(
      //         //     fontSize: screenHeight * 0.08,
      //         //   ),
      //         // ),
      //         // buildText(
      //         //   fontSize: fontSize,
      //         // ),
      //         Expanded(
      //           flex: 6,
      //           child: Padding(
      //             padding: const EdgeInsets.all(70.0),
      //             child: BuildBtn(
      //               fontSize: fontSize,
      //               aspectRatio: aspectRatio,
      //               paddingSize: paddingSize,
      //               textFillWidth: textFillWidth,
      //               textFillHeight: textFillHeight,
      //               fontAnsSize: textInput,
      //             ),
      //           ),
      //         ),
      //       ],
      //     ),
      //   ),
      // );
    }
  }
}
