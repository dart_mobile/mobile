import 'package:flutter/material.dart';

import 'package:high_speed_math/MainGame/mobile_vertical.dart';

import '../GameUi_mobile_horizontal.dart';
import '../Question.dart';

class MobileBodyHorizontal extends StatefulWidget {
  final ValueChanged incrementCounter;
  final ValueChanged randomNumber;
  final int firstNumber;
  final int secondNumber;
  final int score;

  const MobileBodyHorizontal(
      {Key? key,
      required this.score,
      required this.incrementCounter,
      required this.firstNumber,
      required this.secondNumber,
      required this.randomNumber})
      : super(key: key);

  @override
  State<MobileBodyHorizontal> createState() => _MobileBodyHorizontalState();
}

class _MobileBodyHorizontalState extends State<MobileBodyHorizontal> {
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    final fontQusSize = screenHeight * 0.12;
    final fontBtnSize = screenHeight * 0.05;
    final fontAnsSize = screenHeight * 0.05;
    final aspectRatio = (screenHeight / 2) * 0.013;
    final paddingSize = (screenHeight / 2 )* 0.025;
    final textFillWidth = (screenWidth / 2) * 2.5;
    final textFillHeight = screenHeight * 0.1;
    final appbarHeight = screenHeight * 0.12;
    final appbarWidth = screenHeight * 0.125;

    if (screenWidth < screenHeight) {
      return MobileBodyVertical(
        score: widget.score,
        incrementCounter: widget.incrementCounter,
        firstNumber: widget.firstNumber,
        secondNumber: widget.secondNumber,
        randomNumber: widget.randomNumber,
      );
    } else {
      return
          // Container(
          // color: Colors.lightBlue.shade900,
          // child: Row(
          //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          //   children: <Widget>[
          //     Column(
          //       mainAxisAlignment: MainAxisAlignment.center,
          //       children: <Widget>[
          //         Expanded(
          //           flex: 4,
          //           child: buildQuestion(
          //             fontSize: screenHeight * 0.12,
          //             firstNumber: widget.firstNumber,
          //             secondNumber: widget.secondNumber,
          //           ),
          //         ),
          //       ],
          //     ),
          //     Column(
          //       children: <Widget>[
          //         Container(
          //           width: (screenWidth / 2) * 0.88,
          //           height: screenHeight * 0.705,
          //           child: Padding(
          //             padding: const EdgeInsets.only(top: 10, left: 5, right: 5),
          //             child:
          BuildGameMobileHorizontal(
        fontSize: fontAnsSize,
        aspectRatio: aspectRatio,
        paddingSize: paddingSize,
        textFillWidth: textFillWidth,
        textFillHeight: textFillHeight,
        incrementCounter: widget.incrementCounter,
        firstNumber: widget.firstNumber,
        secondNumber: widget.secondNumber,
        score: widget.score,
        randomNumber: widget.randomNumber,
        appbarHeight: appbarHeight,
        appbarWidth: appbarWidth,
      );
    }
  }
}
