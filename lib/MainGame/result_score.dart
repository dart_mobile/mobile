import 'package:flutter/material.dart';

import '../HomPage.dart';

class result_Score extends StatefulWidget {
  final int score;

  const result_Score({Key? key, required this.score}) : super(key: key);

  @override
  State<result_Score> createState() => _result_ScoreState();
}

class _result_ScoreState extends State<result_Score> {
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.lightBlue.shade900,
      body: Center(
        child: Container(
          height: height * 0.3,
          width: height * 0.4,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.lightBlue.shade700,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(
                'Result Score !',
                style: TextStyle(
                    fontSize: height * 0.03,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
              Text(
                '${widget.score}',
                style: TextStyle(
                    fontSize: height * 0.06,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
              GestureDetector(
                onTap: backHome,
                child: Container(
                  padding: EdgeInsets.all(height * 0.005),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Colors.grey.shade100,
                  ),
                  child: Icon(Icons.home,
                      color: Colors.lightBlue.shade700, size: height * 0.04),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void backHome() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => const HomePage()));
  }
}
