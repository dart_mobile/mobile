import 'package:flutter/material.dart';
import 'package:high_speed_math/MainGame/tablet_vertical.dart';

import '../AppBarMath.dart';
import '../GameUi_tablet_horizontal.dart';
import '../GameUi_tablet_vertical.dart';
import '../Question.dart';
import '../Text.dart';
import '../Numpad.dart';

class TabletBodyHorizontal extends StatelessWidget {
  final ValueChanged incrementCounter;
  final ValueChanged randomNumber;
  final int score;
  final int firstNumber;
  final int secondNumber;

  const TabletBodyHorizontal(
      {Key? key,
      required this.score,
      required this.incrementCounter,
      required this.firstNumber,
      required this.secondNumber,
      required this.randomNumber})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    final fontSize = screenHeight * 0.06;
    final aspectRatio = (screenHeight / screenWidth) * 2.3;
    final paddingSize = screenHeight * 0.01;
    final textFillWidth = (screenWidth / 2) * 0.8;
    final textFillHeight = screenHeight * 0.125;
    final appbarHeight = (screenWidth / 2) * 0.10;
    final appbarWidth = screenHeight * 0.125;
    final textInput = screenHeight * 0.04;

    if (screenWidth < screenHeight) {
      return BuildGameTabletVertical(
        fontSize: fontSize,
        firstNumber: firstNumber,
        secondNumber: secondNumber,
        randomNumber: randomNumber,
        appbarWidth: appbarWidth,
        score: score,
        textFillHeight: textFillHeight,
        textFillWidth: textFillWidth,
        paddingSize: paddingSize,
        aspectRatio: aspectRatio,
        appbarHeight: appbarHeight,
        incrementCounter: incrementCounter,
      );
    } else {
      return BuildGameTabletHorizontal(
        fontSize: fontSize,
        firstNumber: firstNumber,
        secondNumber: secondNumber,
        randomNumber: randomNumber,
        appbarWidth: appbarWidth,
        score: score,
        textFillHeight: textFillHeight,
        textFillWidth: textFillWidth,
        paddingSize: paddingSize,
        aspectRatio: aspectRatio,
        appbarHeight: appbarHeight,
        incrementCounter: incrementCounter,);
    }
  }
}
