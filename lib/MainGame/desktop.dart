import 'package:flutter/material.dart';
import 'package:high_speed_math/Question.dart';
import 'package:high_speed_math/Text.dart';

import '../Answer.dart';
import '../AppBarMath.dart';

class DesktopBody extends StatelessWidget {
  final ValueChanged incrementCounter;
  final ValueChanged randomNumber;
  final int firstNumber;
  final int secondNumber;
  final int score;

  const DesktopBody(
      {Key? key,
      required this.score,
      required this.incrementCounter,
      required this.randomNumber,
      required this.firstNumber,
      required this.secondNumber})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    final appbarHeight = screenWidth * 0.04;
    final appbarWidth = 1;

    return Scaffold(
      backgroundColor: Colors.lightBlue[900],
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(appbarHeight as double),
        child: buildAppBar(
          appbarWidth: appbarWidth,
          appbarHeight: appbarHeight,
          score: score,
        ),
      ),
      body: Column(children: <Widget>[
        Expanded(
          flex: 1,
          child: Text(""),
        ),
        Expanded(
          flex: 2,
          child: buildQuestion(
            fontSize: screenWidth * 0.06,
            firstNumber: firstNumber,
            secondNumber: secondNumber,
          ),
        ),
        Expanded(
          flex: 1,
          child: buildText(
            fontSize: screenWidth * 0.015,
          ),
        ),
        Expanded(
          flex: 1,
          child: buildAns(
            textFillWidth: screenWidth * 0.4,
            textFillHeight: screenHeight * 0.085,
            fontSize: screenWidth * 0.02,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 50.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: Container(
                      width: 170,
                      height: 60,
                      child: ElevatedButton(
                        style: ButtonStyle(
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.red.shade400),
                        ),
                        child: Text(
                          'CANCEL',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        onPressed: () {},
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: Container(
                      width: 170,
                      height: 60,
                      child: ElevatedButton(
                        style: ButtonStyle(
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.green.shade400),
                        ),
                        child: Text(
                          'SUMBIT',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        onPressed: () {},
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
        Expanded(
          flex: 1,
          child: Text(""),
        ),
      ]),
    );
  }
}
