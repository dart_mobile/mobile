import 'dart:math';

import 'package:flutter/material.dart';
import 'package:high_speed_math/GameUi_desktop.dart';
import 'package:high_speed_math/reponsive_layout.dart';
import 'MainGame/desktop.dart';
import 'MainGame/mobile_horizontal.dart';
import 'MainGame/mobile_vertical.dart';
import 'MainGame/tablet_horizontal.dart';
import 'MainGame/tablet_vertical.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int score = 0;
  int firstNumber = 3;
  int secondNumber = 8;

  @override
  void initState() {
    super.initState();
    score = score ?? 0;
    firstNumber = firstNumber ?? 0;
    firstNumber = secondNumber ?? 0;
  }

  void _incrementCounter(_) {
    setState(() {
      score++;
      print("score : ${score}");
    });
  }

  void _randomNumber(_) {
    setState(() {
      firstNumber = Random().nextInt(10);
      secondNumber = Random().nextInt(10);
      print("question : ${firstNumber} + ${secondNumber}");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ResponsiveLayout(
        mobileBodyVertical: MobileBodyVertical(
          incrementCounter: _incrementCounter,
          randomNumber: _randomNumber,
          score: score,
          firstNumber: firstNumber,
          secondNumber: secondNumber,
        ),
        mobileBodyHorizontal: MobileBodyHorizontal(
          incrementCounter: _incrementCounter,
          randomNumber: _randomNumber,
          score: score,
          firstNumber: firstNumber,
          secondNumber: secondNumber,
        ),
        tabletBodyVertical: TabletBodyVertical(
          incrementCounter: _incrementCounter,
          randomNumber: _randomNumber,
          score: score,
          firstNumber: firstNumber,
          secondNumber: secondNumber,
        ),
        tabletBodyHorizontal: TabletBodyHorizontal(
          incrementCounter: _incrementCounter,
          randomNumber: _randomNumber,
          score: score,
          firstNumber: firstNumber,
          secondNumber: secondNumber,
        ),
        desktopBody: GameUiDesktop(
          incrementCounter: _incrementCounter,
          randomNumber: _randomNumber,
          score: score,
          firstNumber: firstNumber,
          secondNumber: secondNumber,
        ),
      ),
    );
  }
}
