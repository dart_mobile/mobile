import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'MainGame/result_score.dart';

class buildAppBar extends StatefulWidget {
  final appbarHeight;
  final appbarWidth;
  final score;

  buildAppBar(
      {required this.appbarWidth,
      required this.appbarHeight,
      required this.score});

  @override
  State<buildAppBar> createState() => _buildAppBarState();
}

class _buildAppBarState extends State<buildAppBar> {
  final level = "1";

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      backgroundColor: Colors.lightBlue[700],
      toolbarHeight: widget.appbarHeight,
      title: CountdownTimer(
        textStyle: TextStyle(fontSize: widget.appbarHeight * 0.5),
        endTime: DateTime.now().millisecondsSinceEpoch + 31000,
        // endTime: DateTime.now().millisecondsSinceEpoch + 5000,
        onEnd: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => result_Score(
                    score: widget.score,
                  )));
        },
      ),
      leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.arrow_back_ios,
          size: widget.appbarHeight * 0.5,
        ),
        //replace with our own icon data.
      ),
      actions: <Widget>[
        Padding(
          padding: EdgeInsets.only(right: 20, top: widget.appbarHeight * 0.2),
          child: Text(
            '${widget.score}',
            style: const TextStyle(
                color: Color.fromARGB(255, 255, 255, 255), fontSize: 25),
          ),
        ),
      ],
    );
  }
}
